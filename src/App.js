import React, {Component} from 'react';
import './App.css';
import Item from './components/item/item';
import TextField from "@material-ui/core/TextField";
import ItemSmallWidget from "./components/item/item-small-widget";
import ItemFullInfo from "./components/item/item-full-info";
import gql from "graphql-tag";
import ApolloClient from "apollo-boost";
import IconButton from "@material-ui/core/IconButton";
import {Add} from "@material-ui/icons";

const client = new ApolloClient({
    uri: 'http://localhost:4000/'
});

class App extends Component {
    state = {
        cities: [
            {name: 'Ivanovo', countryCode: 'ru', favorite: false, id: 1},
            {name: 'Vladimir', countryCode: 'ru', favorite: false, id: 2},
            {name: 'Yaroslavl', countryCode: 'ru', favorite: false, id: 3},
            {name: 'Kostroma', countryCode: 'ru', favorite: false, id: 4}
        ],
        citiesTemperature: [],
        fullWeatherInfo: null
    };
    newCityName = '';
    newCityCode = '';

    toggleFavorite = (index) => {
        const cities = [...this.state.cities];
        cities[index].favorite = !cities[index].favorite;

        this.setState({cities: cities});
        if (cities[index].favorite) {
            this.getFavoredCityInfo(cities[index]);
        } else {
            const temperatureInfo = [...this.state.citiesTemperature];
            this.setState({citiesTemperature: temperatureInfo.filter(city => city.id !== cities[index].id)});
        }
    };

    deleteCity = (personIndex) => {
        const cities = [...this.state.cities];
        let deletedElement = cities.splice(personIndex, 1);
        const favoriteCities = [...this.state.citiesTemperature];
        this.setState({cities: cities});
        this.setState({citiesTemperature : favoriteCities.filter(city => city.id !== deletedElement[0].id)});
    };

    addCity = () => {
        if (this.newCityName !== '') {
            const cities = [...this.state.cities];

            cities.sort((city1, city2) => {
                switch (city1.id < city2.id) {
                    case true: {
                        return 1
                    }
                    case false: {
                        return -1
                    }
                    default: {
                        return 0
                    }
                }
            });
            cities.push({
                name: this.newCityName,
                countryCode: this.newCityCode,
                favorite: false,
                id: cities.length > 0 ? cities[0].id + 1 : 0
            });

            this.setState({cities: cities});
        }
    };

    changeNewCityName = (e) => {
        this.newCityName = e.target.value;
    };

    changeNewCityCode = (e) => {
        this.newCityCode = e.target.value;
    };

    showFullInfo = (city) => {
        this.geFullWeatherInfo(city);
    };

    getFavoredCityInfo = (city) => {
        let updatedCitiesInfo = [...this.state.citiesTemperature];
        let cityInfo = {
            id: city.id,
            name: city.name,
            temperatureC: 'Нет данных',
            temperatureF: 'Нет данных',
            temperatureK: 'Нет данных'
        }
        let query = gql`
            query getCurrentWeather($cityName: String, $countryCode: String) {
                getCurrentWeather(cityName: $cityName, countryCode: $countryCode) {
                    main{temp, temp_c, temp_f}
                }
            }`;

        client
            .query({
                query: query,
                variables: {
                    cityName: city.name,
                    countryCode: city.countryCode
                }
            })
            .then(result => {
                if (result && result.data && result.data.getCurrentWeather && result.data.getCurrentWeather.main) {
                    cityInfo.temperatureC = result.data.getCurrentWeather.main.temp_c ? result.data.getCurrentWeather.main.temp_c : "Нет данных";
                    cityInfo.temperatureF = result.data.getCurrentWeather.main.temp_f ? result.data.getCurrentWeather.main.temp_f : "Нет данных";
                    cityInfo.temperatureK = result.data.getCurrentWeather.main.temp_c ? result.data.getCurrentWeather.main.temp_c : "Нет данных";
                    updatedCitiesInfo.push(cityInfo);
                    this.setState({
                        citiesTemperature: updatedCitiesInfo
                    });
                }
            })
            .catch(err => {
                cityInfo.temperatureC = "Нет данных";
                cityInfo.temperatureF = "Нет данных";
                cityInfo.temperatureK = "Нет данных";
                updatedCitiesInfo.push(cityInfo);
                this.setState({
                    citiesTemperature: updatedCitiesInfo
                });
                console.log(err);
            });
    };

    geFullWeatherInfo = (city) => {
        let fullWeatherInfo = null;
        let query = gql`
            query gettWeather($cityName: String, $countryCode: String) {
                getWeather(cityName: $cityName, countryCode: $countryCode) {
                    list {
                        dt
                        main {
                            pressure
                            humidity
                        }
                        weather {
                            description
                            icon
                        }
                        clouds {
                            all
                        }
                        wind {
                            speed
                            deg
                        }
                        dt_txt
                    },
                    city{
                        name
                        country
                        coord {
                            lat
                            lon
                        }
                        sunrise
                        sunset
                    },
                    temp_celcius,
                    celcius_avg
                }
            }`;

        client
            .query({
                query: query,
                variables: {
                    cityName: city.name,
                    countryCode: city.countryCode
                }
            })
            .then(result => {
                if (result && result.data && result.data.getWeather) {
                    this.setState({
                        fullWeatherInfo: result.data.getWeather
                    });
                }
            })
            .catch(err => {
                this.setState({
                    fullWeatherInfo: fullWeatherInfo
                });
                console.log(err);
            });
    };

    render() {
        return (
            <div className='main-page-shell'>
                <div className='cities-list'>
                    <div className='control-panel'>
                        <TextField id="standard-secondary" onChange={this.changeNewCityName} label="Новый город"
                                   color="primary"/>
                        <TextField id="standard-secondary" onChange={this.changeNewCityCode} label="Код города"
                                   color="primary"/>
                        <IconButton type="button" className='AddCircleOutline' aria-label="search" onClick={this.addCity}>
                            <Add />
                        </IconButton>
                    </div>
                    <div>
                        {this.state.cities.map((city, index) => {
                            return <Item
                                key={city.id}
                                click={() => this.toggleFavorite(index)}
                                clickDelete={() => this.deleteCity(index)}
                                clickFullInfo={() => this.showFullInfo(city)}
                                name={city.name}
                                favorite={city.favorite}/>
                        })}
                    </div>
                </div>
                <div className='favoreds-information'>
                    <ItemFullInfo fullInfo={this.state.fullWeatherInfo}/>
                    <ItemSmallWidget citiesInfo={this.state.citiesTemperature}/>
                </div>
            </div>
        );

    }
}

export default App;
