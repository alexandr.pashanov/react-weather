import React from 'react';
import StarIcon from '@material-ui/icons/Star';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import Delete from "@material-ui/icons/Delete"
import IconButton from "@material-ui/core/IconButton";

const Item = (props) => {
    return (
        <List component="nav" aria-label="contacts">
            <IconButton type="button" className='AddCircleOutline' aria-label="search" onClick={props.click}>
                    {props.favorite === false &&
                    <StarIcon/>
                    }
                    {props.favorite === true &&
                    <StarIcon style={{color:'gold'}}/>
                    }
            </IconButton>

            <ListItem onClick={props.clickFullInfo} button>
                <ListItemText  inset primary={props.name}/>
            </ListItem>
            <IconButton type="button" className='AddCircleOutline' aria-label="search" onClick={props.clickDelete}>
                <Delete/>
            </IconButton>
        </List>
    );
};

export default Item;
