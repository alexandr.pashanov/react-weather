import React from 'react';
import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import {LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip} from 'recharts';
import './item-full-info.css';

class ItemFullInfo extends React.Component {
    render() {
        const data = this.props.fullInfo;
        let graphData = [];
        if (data) {
            graphData = data.list.map((entry, index) => {
                return {
                    name: Intl.DateTimeFormat('ru-RU', {
                        year: "numeric",
                        month: "short",
                        day: "2-digit",
                        hour: "numeric",
                        minute: "2-digit",
                        second: "2-digit"
                    }).format(entry.dt + '000'),
                    Temperature: data.temp_celcius[index]
                }
            })
        }

        return (
            <div className="full-info-root">
                <div className="left-side">
                    <div className='title'>Погода в {data ? data.city.name : ''} {data ? data.city.country : ''}</div>
                    <div className='temperature'>{data ? data.celcius_avg : ''}C°</div>
                    <div className='description'>{data ? data.list[0].weather[0].description : ''}</div>
                    <div className='time'>{
                        data ? data.list[0].dt_txt : ''
                    }</div>
                    <div className='infoTable'>
                        <TableContainer>
                            <Table aria-label="simple table">
                                <TableBody>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Ветер
                                        </TableCell>
                                        <TableCell align="right">
                                            <div>Скорость: {data ? data.list[0].wind.speed : ''} м/с</div>
                                            <div>Направление: {data ? data.list[0].wind.deg : ''}°</div>
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Облачность
                                        </TableCell>
                                        <TableCell align="right">
                                            <div>{data ? data.list[0].clouds.all : ''}%</div>
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Давление
                                        </TableCell>
                                        <TableCell align="right">
                                            <div>{data ? data.list[0].main.pressure : ''} мм.рт.ст.</div>
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Влажность
                                        </TableCell>
                                        <TableCell align="right">
                                            <div>{data ? data.list[0].main.humidity : ''} мм.рт.ст.</div>
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Восход
                                        </TableCell>
                                        <TableCell align="right">
                                            <div>{data
                                                ?
                                                Intl.DateTimeFormat('ru-RU', {
                                                    year: "numeric",
                                                    month: "short",
                                                    day: "2-digit",
                                                    hour: "numeric",
                                                    minute: "2-digit",
                                                    second: "2-digit"
                                                }).format(data.city.sunrise + '000')
                                                : ''}
                                            </div>
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Закат
                                        </TableCell>
                                        <TableCell align="right">
                                            <div>{data
                                                ?
                                                Intl.DateTimeFormat('ru-RU', {
                                                    year: "numeric",
                                                    month: "short",
                                                    day: "2-digit",
                                                    hour: "numeric",
                                                    minute: "2-digit",
                                                    second: "2-digit"
                                                }).format(data.city.sunset + '000')
                                                : ''}
                                            </div>
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Координаты
                                        </TableCell>
                                        <TableCell align="right">
                                            <div>Широта: {data ? data.city.coord.lat : ''}°</div>
                                            <div>Долгота: {data ? data.city.coord.lon : ''}°</div>
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                </div>
                <div className='root-side'>

                    <LineChart width={1180} height={492} data={graphData}
                               margin={{top: 5, right: 20, bottom: 5, left: 0}}>
                        <Line type="monotone" dataKey="Temperature" stroke="#8884d8"/>
                        <CartesianGrid stroke="#ccc" strokeDasharray="5 5"/>
                        <XAxis dataKey="name"/>
                        <YAxis/>
                        <Tooltip/>
                    </LineChart>
                </div>
            </div>
        )
    }
}

export default ItemFullInfo;
