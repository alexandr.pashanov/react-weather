import React from 'react';
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody";

class ItemSmallWidget extends React.Component{
    render(){
        return (
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center">Город</TableCell>
                            <TableCell align="center">Температура C</TableCell>
                            <TableCell align="center">Температура F</TableCell>
                            <TableCell align="center">Температура K</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.citiesInfo.map((city, index) => {
                            console.log(city)
                            return <TableRow key={index}>
                                <TableCell align="center" component="th" scope="row">
                                    {city.name}
                                </TableCell>
                                <TableCell align="center">{city.temperatureC}</TableCell>
                                <TableCell align="center">{city.temperatureF}</TableCell>
                                <TableCell align="center">{city.temperatureK}</TableCell>
                            </TableRow>
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        );
    }
}

export default ItemSmallWidget;
